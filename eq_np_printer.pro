TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

TARGET = eq_np_printer

INCLUDEPATH += /usr/include/opencv/

SOURCES += \
	main.c

unix:!macx: LIBS += -lopencv_core -lopencv_imgproc -lopencv_highgui
unix:!macx: LIBS += /usr/lib/libNPrint.so.1
