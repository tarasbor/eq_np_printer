/*
 ============================================================================
 Name        :libNPrint_sample.c
 Author      : Nippon Printer Engnieering Inc.
 Version     :
 Copyright   :
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <time.h>		// localtime
#include <cv.h>			// OpenCV
#include <highgui.h>	// OpenCV

#include <stdarg.h>     // for LOG

#include <sys/types.h> // for isDirAndExist
#include <sys/stat.h>
#include <unistd.h>
#if SC_PLATFORM == SC_PLATFORM_LINUX
#include <errno.h>
#endif

#include <dirent.h> //for DIR*

//#include <sys/types.h>	// open
//#include <sys/stat.h>	//
//#include <fcntl.h>		//
//#include <termios.h>	// termios

const char cVersion[]="Version 0.11 \n";
const char cLogFileLocation[]="/var/log/npi.log";

#define	SUCCESS					0
#define	ERR_OFFLINE				-5

#define IMG_RASTER_LINE				0x00
#define IMG_RASTER_BLOCK			0x01
#define IMG_RASTER_GRADATION		0x02
#define IMG_BITIMG					0x10

#define F309_CUT_SMALL      "1b4bff1b69"
#define F309_CUT_STANDARD   "1b4aff1b69"
#define WAIT_PRINT_COMMAND_SEND 2000
#define WAIT_PRINT_COMPLETE     2000
#define WAIT_IN_MICROSEC        200000
#define MAX_F309IMAGE_WIDTH     576

#define LOG_ERROR 5

//#define LOG_DEBUG_VERBOSE 1

#define FILESLISTMAXSIZE 100



FILE *gmErrorlogFile;


// Loggging
void _proxy_log(int level, const char *fmt, ...)
    __attribute__((format (printf, 2, 3)));

#define proxy_log(level, fmt, ...) _proxy_log(level, fmt"\n", ##__VA_ARGS__)

void _proxy_log(int level, const char *fmt, ...) {
    va_list arg;
    FILE *log_file = gmErrorlogFile;
    char time_now[32];
    memset(time_now,0,32);
    fncGetDate(time_now);

    /* Check if the message should be logged */
//    if (level > log_level)
//        return;

    fprintf(log_file,"%s ",time_now);
    /* Write the error message */
    va_start(arg, fmt);
    vfprintf(log_file, fmt, arg);
    va_end(arg);

//#ifdef DEBUG
    fflush(log_file);
    fsync(fileno(log_file));
//#endif
}


//***************************************************************************
//  Name : fncGetDate
//***************************************************************************
void fncGetDate(char *o_staData)
{
    time_t timep;
    struct tm *time_inf;
    char staDate[32];

    time(&timep);

    time_inf = localtime(&timep);
    memset(staDate, 0x00, 32);
    sprintf(staDate,"%04d.%02d.%02d %02d:%02d:%02d", time_inf->tm_year + 1900, time_inf->tm_mon + 1, time_inf->tm_mday, time_inf->tm_hour, time_inf->tm_min, time_inf->tm_sec);
    memcpy(o_staData, staDate, strlen(staDate));
    return;
}

// --------------
// true         - if dir exist
// false        - if it is not dir or not exist
// terminate    - if stat not work
bool isDirAndExist(char* folder)
{
    struct stat s;
    int err = stat(folder, &s);
    if(-1 == err) {
        if(ENOENT == errno) {
            /* does not exist */
            return false;
        } else {
            _proxy_log(LOG_ERROR,"isDirAndExist, error stat()");
            perror("error stat()");
            exit(1);
        }
    } else {
        if(S_ISDIR(s.st_mode)) {
            /* it's a dir */
            return true;
        } else {
            /* exists but is no dir */
            return false;
        }
    }
}

int fncConnectToPrinter(char* staPort)
{
    // ポートオープン
    // Port open
    int  nmsRet = NOpenPrinter(staPort, 1);
    _proxy_log(LOG_ERROR,"NOpenPrinter: %d\n", nmsRet);
    usleep(2000000);

    return nmsRet;
}

int fncPrinterReady(char* staPort)
{
    int nmuStatus;
    int nmsRet = NGetStatus(staPort, &nmuStatus);
#ifdef LOG_DEBUG_VERBOSE
    printf(" NGetStatus: %d -> Status : %02lX\n", nmsRet, nmuStatus);
    _proxy_log(LOG_ERROR," NGetStatus: %d -> Status : %02lX\n", nmsRet, nmuStatus);
#endif

    return nmsRet;
}


//---------------------------------------------------------------------------
//  Name     : fncDriverPrintImage
//  Deteail  : print image
//  Argument :
//  Return   :
//---------------------------------------------------------------------------
int fncDriverPrintImage(char* portName, char *imageToPrint, bool small, char interpol)
{
    char staPort[8];
    unsigned char rawGetData[128];
    char staDate[32];

    int nmsRet;
    unsigned long nmuTimeout;
    unsigned long nmuTime;
    unsigned long nmuStatus;
    unsigned int nmuJob;
    unsigned int nmuGetJobID;
    unsigned int *pnmuID;

    //char *staImgPath = "/home/nei/npi/img/PrintSample.bmp";
    //char *staImgPath = "/home/taras/temp.png";
    //IplImage *img = cvLoadImage("/home/nei/npi/img/PrintSample.bmp", CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
    IplImage *sourceImg = cvLoadImage(imageToPrint, CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
    if(sourceImg==0)
    {
        printf("Error while loading image '%s' \n", imageToPrint);
        return -1000;
    }
    IplImage *img=cvCreateImage(cvSize(MAX_F309IMAGE_WIDTH, sourceImg->height), sourceImg->depth, sourceImg->nChannels);
    cvResize(sourceImg, img, interpol); // or similar CV_INTER_LANCZOS4

    memset(staDate, 0x00, 32);
    fncGetDate(staDate);
    printf("%s **** begin print ****\n",staDate);
    memset(staPort, 0x00, 8);
    memcpy(staPort, portName, 6);
    printf("Input port name: %s\n",staPort);



//    connectToPrinter(staPort);



/*    // 年月日、時間取得
    // get a date
    memset(staDate, 0x00, 32);
    strcat(staDate, "\"DATE : ");
    fncGetDate(&staDate[strlen(staDate)]);
    strcat(staDate, "\"");  */

    // 状態確認
    // Printer satatus check
//    nmsRet = NGetStatus(staPort, &nmuStatus);
//    printf(" NGetStatus             ReturnCode : %d -> Status           : %02lX\n", nmsRet, nmuStatus);

//    if(nmsRet != SUCCESS)
//    {
//        return nmsRet;
//    }

    // 情報取得CMD発行
    // Information acquisition command output
    //nmsRet = NPrint(staPort, "1b73021b73031b7304", strlen("1b73021b73031b7304"), &nmuJob);

   // usleep(WAIT_IN_MICROSEC);

    //
    // データセット
    // Set data
    //

    // StartDoc
    nmuJob = 0;
    nmsRet = NStartDoc(staPort, &nmuJob);
    printf(" NStartDoc              ReturnCode : %d -> JOB ID : %d\n", nmsRet, nmuJob);
    if(nmsRet != SUCCESS)
    {
        return nmsRet;
    }

    // Raster image data
    printf("img '%s' width %d height %d", imageToPrint, img->width, img->height);
    nmsRet = NImagePrint(staPort, img, img->width, img->height, IMG_RASTER_BLOCK, NULL);
    // 文字データ(Feed&Cut)
    // Feed / Cut command
    //      1b4aff1b69      // standard size
    //      1b4bff1b69      // small size
    char* cutcommand;
    cutcommand = small ? F309_CUT_SMALL : F309_CUT_STANDARD;
    nmsRet = NPrint(staPort, cutcommand, strlen(cutcommand), NULL);


    // 印字終了CMD
    // Out printing command
    nmsRet = NPrint(staPort, "1d4710", strlen("1d4710"), NULL);

    // EndDoc
    nmsRet = NEndDoc(staPort);
    printf(" NEndDoc                ReturnCode : %d\n", nmsRet);
    if(nmsRet != SUCCESS)
    {
        nmsRet = NCancelDoc(staPort);
        printf(" NCancelDoc             ReturnCode : %d\n", nmsRet);
        return nmsRet;
    }

    // 転送完了確認
    // It waits until forwarding is completed.
    nmuGetJobID = 0;
    nmuTimeout = cputime();
    while(nmuGetJobID != nmuJob)
    {
        memset(rawGetData, 0x00, 128);
        nmsRet = NGetInformation(staPort, (unsigned char)25, rawGetData, &nmuTime);
        pnmuID = (unsigned int*)&rawGetData;
        nmuGetJobID = *pnmuID;

        if(cputime() > (nmuTimeout+WAIT_PRINT_COMMAND_SEND))
        {
            printf(" Print Error(ID:25)\n");
            break;
        }
    }
    printf(" ID:25 GetJobID:%d\n", nmuGetJobID);
    if(nmuGetJobID == nmuJob)
    {
        // 印刷完了待ち
        // It waits until printing is completed.
        nmuGetJobID = 0;
        nmuTimeout = cputime();
        while(nmuGetJobID != nmuJob)
        {
            memset(rawGetData, 0x00, 128);
            nmsRet = NGetInformation(staPort, (unsigned char)19, rawGetData, &nmuTime);
            pnmuID = (unsigned int*)&rawGetData;
            nmuGetJobID = *pnmuID;

            if(cputime() > (nmuTimeout+WAIT_PRINT_COMPLETE))
            {
                printf(" Print Error(ID:19)\n");
                break;
            }
        }
        if(nmuGetJobID == nmuJob)
        {
            printf(" ID:19 GetJobID:%d\n", nmuGetJobID);
            printf(" Print Success\n");
        }
    }

    // ポートクローズ

    fncGetDate(staDate);
    printf("%s **** finish print ****\n", staDate);

    cvReleaseImage(&img);

    return nmsRet;
}

int start()
{

    gmErrorlogFile = fopen(cLogFileLocation,"a");
    if(gmErrorlogFile == NULL)
    {
        printf("Error opening file '%s' for write.\n", cLogFileLocation);
        return 0;
    }

    _proxy_log(LOG_ERROR,"------------\n");
    _proxy_log(LOG_ERROR,cVersion);
    printf(cVersion);
}

int shutdown()
{


    if(gmErrorlogFile)
        fclose(gmErrorlogFile);
}



//***************************************************************************
//  Name : main
//***************************************************************************
int main(int argc, char *argv[])
{

    if (start()<0)
        return 0;


    if (argc<1) {
        printf("Not enough parameters. \n");
        printf("Use -h for help. \n");
        return 0;
    }

    bool small = false;
    char inter = CV_INTER_CUBIC;
    char* port="PRT001";
    char* imagesFolder="";


    for(int i=1; i<argc; i++)
    {
        char* argument = argv[i];
        if(strcasecmp(argument,"--help")==0)
        {
            printHelp();
            return 0;
        }

        if((strlen(argument)==2 && argument[0]=='-')){  //command
            switch(argument[1]){
            case 'l': {small=false; inter=CV_INTER_CUBIC; } break;
            case 's': {small=true; inter=CV_INTER_CUBIC; } break;
            case 'm': {small=true; inter=CV_INTER_AREA; } break;
            case 'i': {
                if(i==argc-1) {
                    printf("No image. \n");
                    printf("Use -h for help. \n");
                    return 0;
                }
                imagesFolder=argv[i+1];
            } break;
            case 'p': {
                if(i==argc-1) {
                    printf("No port. \n");
                    printf("Use -h for help. \n");
                    return 0;
                }
                port=argv[i+1];
            } break;

            case 'h': {
                printHelp();
                return 0;
            } break;
            case 'c': { // Enum Printers
                int nmsRet;
                char *pPrinters;
                unsigned int nmuSize;
                unsigned int nmuDummy;

                nmuSize = 0;
                nmsRet = NEnumPrinters(NULL, &nmuSize);
                if(nmsRet<0)
                {
                    printf("Error. NEnumPrinters(ret:%d)\n", nmsRet);
                    printf("Maybe not enough rights, or not exist folder '/usr/npi/'. \n");
                    return 0;
                }
                if(nmuSize != 0)
                {
                    pPrinters = (char*)malloc(sizeof(char)*(nmuSize+1));
                    memset(pPrinters, 0x00, sizeof(char)*(nmuSize+1));
                    NEnumPrinters(pPrinters, &nmuDummy);
                    printf("%s\n", pPrinters);
                    free(pPrinters);
                }
                return 0;
            } break;
        } //switch
        } //if
    } //for


    if(strlen(imagesFolder)<1)    {
        printf("No image folder specified. \n");
        printf("Use -h for help. \n");
        _proxy_log(LOG_ERROR,"No image folder specified. \n");
        return 0;
    }


    bool bConnected=false, bReady=false;
    while(1)
    {
        // check status
        if(!bReady) {
            // connect if neccesary
            if(!bConnected) {
                if(fncConnectToPrinter(port)!=SUCCESS) {
                    usleep(10000000);
                    continue;
                } else bConnected = true;
            }

            if(fncPrinterReady(port)!=SUCCESS) {
                // port close
                if(bConnected) {
                    int nmsRet = NClosePrinter(port);
                    printf(" NClosePrinter : %d\n", nmsRet);
                    _proxy_log(LOG_ERROR," NClosePrinter : %d\n",nmsRet);
                    bReady=false;
                }
                bConnected=false;
                usleep(10000000);
                continue;
            } else bReady = true;
        }

        // check folder
        if(isDirAndExist(imagesFolder)) {

             /// list files
             DIR           *d;
             struct dirent *dir;
             d = opendir(imagesFolder);
             if (d)
             {
                 // fill array of files
                 struct fileslist{
                     char filename[FILENAME_MAX];
                     int datemodified;
                 }fLt[FILESLISTMAXSIZE];
                 int fLtcount=0;

                 while (((dir = readdir(d)) != NULL) /*&& (dir->d_type == DT_REG)*/)
                 {
                     if(dir->d_type == DT_REG && dir->d_name[0]!='.') {    // regular file & not hidden
                         char fname[FILENAME_MAX];
                         memset(fname,0,FILENAME_MAX);
                         memcpy(fname, imagesFolder, strlen(imagesFolder));                 // no check > MAX
                         memcpy(fname+strlen(imagesFolder),dir->d_name, strlen(dir->d_name));
                         struct stat st = {0};
                         int ret = lstat(fname, &st);
                         if(ret == -1)
                         {
                             perror("lstat");
                             _proxy_log(LOG_ERROR,"lstat error\n");
                             break;
                         }

                         //add to struc
                         memset(fLt[fLtcount].filename,0,FILENAME_MAX);
                         memcpy(fLt[fLtcount].filename,fname,strlen(fname));
                         fLt[fLtcount].datemodified = st.st_mtim.tv_sec;
                         fLtcount++;
                         if(fLtcount>=FILESLISTMAXSIZE)
                             break;
#ifdef LOG_DEBUG_VERBOSE
                         printf("%s  mtime: %d\n", dir->d_name, fLt[fLtcount-1].datemodified);    //debug
#endif
                     }
                 } // while
                 closedir(d);

                 /// sort files by datemodified desc
                 for (int j=1; j<fLtcount-1; j++){
                     bool f=false;
                     for (int i=1; i<fLtcount-j; i++)
                     {
                         if(fLt[i].datemodified>fLt[i+1].datemodified)
                         {
                             //swap
                             char fname[FILENAME_MAX];
                             memset(fname,0,FILENAME_MAX);
                             memcpy(fname, fLt[i].filename, strlen(fLt[i].filename));
                             int datemodified = fLt[i].datemodified;
                             memset(fLt[i].filename,0,FILENAME_MAX);
                             memcpy(fLt[i].filename, fLt[i+1].filename, strlen(fLt[i+1].filename));
                             fLt[i].datemodified=fLt[i+1].datemodified;
                             memset(fLt[i+1].filename,0,FILENAME_MAX);
                             memcpy(fLt[i+1].filename, fname, strlen(fname));
                             fLt[i+1].datemodified = datemodified;
                         }
                         f=true;
                     }
                     if(!f) break;
                 }


#ifdef LOG_DEBUG_VERBOSE
                 for(int i=0; i<fLtcount; i++)
                     printf("%s  mtime: %d\n", fLt[i].filename, fLt[i].datemodified);    //debug
#endif

                 // print
                 if(bReady && fLtcount!=0)
                 {
                     int nmsRet = fncDriverPrintImage(port, fLt[fLtcount-1].filename, small, inter);
                     printf("fncDriverPrintImage: %d \n", nmsRet);
                     _proxy_log(LOG_ERROR," fncDriverPrintImage : %d\n", nmsRet);
                     if(nmsRet!=SUCCESS)
                     {
                         if(bConnected){     //port close
                             // port close
                             nmsRet = NClosePrinter(port);
                             printf(" NClosePrinter : %d\n", nmsRet);
                             _proxy_log(LOG_ERROR," NClosePrinter : %d\n",nmsRet);
                             bConnected=false;
                             bReady=false;
                         }
                         usleep(10000000);
                         continue;
                     }
                     // delete
                     nmsRet = remove(fLt[fLtcount-1].filename);
                     printf("remove '%s': %d \n", fLt[fLtcount-1].filename, nmsRet);
                     _proxy_log(LOG_ERROR," remove '%s' : %d\n",fLt[fLtcount-1].filename, nmsRet);
                     if(nmsRet!=SUCCESS)   //fatal error. Can`t delete file!
                     {
                         printf("remove : %d\n", nmsRet);
                         _proxy_log(LOG_ERROR,"Fatal, remove : %d\n", nmsRet);
                         break;
                     }
                 }

             } else { // if (d)
                 _proxy_log(LOG_ERROR,"Error opendir '%s'. \n",imagesFolder);
                 printf("Error opendir '%s'.\n",imagesFolder);
                 break;
             }
             //break; // uncomment for debug

        }else{ // if(isDirAndExist
             _proxy_log(LOG_ERROR,"Folder for images '%s' not exist or not a folder.\n",imagesFolder);
             printf("Folder for images '%s' not exist or not a folder.\n",imagesFolder);
             break;
        }


        bReady=false;
        usleep(300000);
    } // while

    /*int ret = fncDriverPrintImage(port,image, small, inter);
    if (ret<0)
        printf("Failed: %d \n",ret);*/

    if(bConnected){     //port close
        // port close
        int nmsRet = NClosePrinter(port);
        printf(" NClosePrinter : %d\n", nmsRet);
        _proxy_log(LOG_ERROR," NClosePrinter : %d\n");
        bConnected=false;
        bReady=false;
    }

    shutdown();

    // 入力させる
    return EXIT_SUCCESS;
}

void printHelp()
{
    const char progname[]="eq_np_printer";
    printf("Usage:\n %s -[l|s|m] [-p <port>] -i <image>\n", progname);
    printf(" %s -c \n", progname);
    printf("Also need write permission to '/var/log/npi.log' file.\n");
    printf("\n");
    printf("  -c \t\tgenerate printer managment file under 'usr/npi/'\n\t\t  need write permission\n");
    printf("  -i \t\tcatalog with images, with write permission (mandatory)\n");
    printf("  -p \t\tprinter port(default PRT001), from printer managment file (optional)\n");
    printf("  -l \t\tprint long format, this default\n");
    printf("  -s \t\tprint small format\n");
    printf("  -m \t\tprint small format with transition\n\t\t  bottom 1/8 part to upper part\n");
}


